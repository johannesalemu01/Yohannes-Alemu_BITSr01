const texts = ["Student", "FrontEnd Developer", "Web Developer"];
const colors = ["#1e9bffcc", "#00ff00", "#feffca", "#1098ad"];
let index = 0;
let letterIndex = 0;
let currentText = "";
let currentColor = "";
let isDeleting = false;
function type() {
  currentText = texts[index];
  currentColor = colors[index];
  const multipleText = document.getElementById("multiple-text");
  const multiWrap = document.querySelector(".multi-wrap");
  if (!isDeleting) {
    multipleText.innerHTML = currentText.substring(0, letterIndex + 1);
    multipleText.style.color = currentColor;
    multipleText.style.textDecoration = "underline 3px ";
    letterIndex++;
    if (letterIndex === currentText.length) {
      isDeleting = true;
      if (multiWrap.innerHTML == "") {
        multiWrap.style.visibility = "hidden";
      }
      setTimeout(type, 600);
    } else {
      setTimeout(type, 200);
    }
  } else {
    multipleText.innerHTML = currentText.substring(0, letterIndex - 1);
    letterIndex--;
    if (letterIndex === 0) {
      isDeleting = false;
      index = (index + 1) % texts.length;
    }
    setTimeout(type, 100);
  }
}
type();

// toggle
const toggleBarOpen = document.getElementById("toggle-bar-open");
const toggleBarClose = document.getElementById("toggle-bar-close");
const header = document.getElementById("header");
const container = document.querySelector(".container");
const copy = document.querySelector(".copy");
const headImage = document.querySelector(".head-image");
toggleBarOpen.addEventListener("click", () => {
  container.style.marginLeft = "300px";
  header.style.left = "0";
  header.style.display = "flex";
  copy.style.display = "none";
  document.getElementById("contact").style.marginBottom = "0";
  document.querySelector(".head-image").style.flexDirection = "column";
  document.querySelector(".head-image").style.alignItems = "center";
  document.querySelector(".head-image").style.justifyContent = "center";
  // document.querySelector(".head-image").childNodes.style.width = "70%";
  toggleBarOpen.style.display = "none";
  toggleBarClose.style.display = "inline-flex";
});
toggleBarClose.addEventListener("click", () => {
  container.style.marginLeft = "0";
  // header.style.left = "-300px";
  header.style.display = "none";
  copy.style.display = "block";
  document.getElementById("contact").style.marginBottom = "40px";
  headImage.style.flexDirection = "row";
  toggleBarOpen.style.display = "inline-flex";
  toggleBarClose.style.display = "none";
});

const toggleBtn = document.querySelector(".toggle");
toggleBtn.addEventListener("click", () => {
  toggleBtn.style.background =
    toggleBtn.style.background == "white" ? "black" : "white";
  themeToggleBtn.style.color =
    themeToggleBtn.style.color == "black" ? "white" : "black";
});
// dark-mode
const themeToggleBtn = document.querySelector(".theme-toggle");
const theme = localStorage.getItem("theme");
if (theme) {
  // hero.style.backgroundImage = "unset";
  container.classList.add(theme);
}

themeToggleBtn.addEventListener("click", () => {
  container.classList.toggle("dark-mode");
  if (container.classList.contains("dark-mode")) {
    localStorage.setItem("theme", "dark-mode");
  } else {
    localStorage.removeItem("theme");
  }
});

window.addEventListener("scroll", function () {
  var heroPage = document.getElementById("hero");
  var backToTopButton = document.querySelector(".back-to-top");

  // Check if user is on the hero page
  var isHeroPage = window.scrollY < heroPage.clientHeight;

  if (isHeroPage) {
    backToTopButton.style.display = "none";
  } else {
    backToTopButton.style.display = "flex";
  }
});

document.querySelector(".back-to-top").addEventListener("click", function () {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
});

// managing actibe bar

window.addEventListener("scroll", function () {
  var sections = document.querySelectorAll("section");
  var navLinks = document.querySelectorAll(".nav-link");

  sections.forEach(function (section, index) {
    var top = section.offsetTop;
    var height = section.clientHeight;
    if (window.pageYOffset >= top && window.pageYOffset < top + height) {
      navLinks[index].classList.add("active");
    } else {
      navLinks[index].classList.remove("active");
    }
  });
});
